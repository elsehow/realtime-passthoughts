var $         = require('jquery')
  , postJSON  = require('post-json-nicely')
  , Kefir     = require('kefir')

var setEnabled = function ($el, bool) { $el.prop('disabled', !bool) }
  , bothTruthy = function (x,y) { if (x && y) { return true } else { return false } }
  
var emit = function (msg) { 
  postJSON($, 'http://indra.webfactional.com', msg) 
  console.log('emitting..', msg)
}

var setMessagePreview = function (msg) {
  $('#messagePreview').html(JSON.stringify(msg, undefined, 2))
}

var recordingMessage = function (v) {
  return {
    type: 'start-recording', 
    tag: v[0], 
    duration: v[1],
    success_message: {
      type: 'done-recording',
      tag: v[0],
      duration: v[1],
    }
  }
}

var fieldStream = function ($el, evname) {
  return Kefir.fromEvents($el, evname)
              .debounce(100)
              .map(function (ev) { return ev.target.value })
}

function setup () {
  // start button should be disabled unless those are filled in
  var $startButton = $('#startButton')
  var startClicks = Kefir.fromEvents($startButton, 'click')
  var tag = Kefir.merge([fieldStream($('#tag'), 'keypress'),
                         fieldStream($('#tag'), 'change')]).skipDuplicates()
  var duration = fieldStream($('#duration'), 'change')
  var buttonEnabledStream = Kefir.combine([tag, duration], bothTruthy)
                                 .toProperty(function() { return false })
  var submissionStream = Kefir.combine([tag, duration])
                              .map(recordingMessage)

  //side effects
  // toggle button enabled/disabled
  buttonEnabledStream.onValue(function (v) { setEnabled($startButton, v) })
  // draw message preview on the UI
  submissionStream.onValue(setMessagePreview)
  // submit messages on button clicks
  submissionStream.sampledBy(startClicks).onValue(emit)
}

$(document).on('ready', setup)