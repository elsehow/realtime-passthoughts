var _              = require('lodash')
var r              = require('rethinkdb')
var request        = require('request-json')
// db
var my_host        = '192.168.99.100'
var my_port        = 32769
var my_db          = 'passthoughts'
var my_table       = 'enrollment'
var db_config      = { host: my_host, port: my_port, db: my_db }
// pusher
var hostname   = 'http://indra.webfactional.com'
var faucet     = require('socket.io-client')(hostname)
// messages
var indra_server   = request.createClient('http://indra.webfactional.com')
var post           = function (msg) { indra_server.post('/', msg.success_message, function (err, res, parsed) {return }) }

// connect to database
r.connect(db_config, function(err, conn) {

  if (err) throw err

  var insert = function (d, cb) {
    r.table(my_table).insert(d).run(conn, cb)
  }

  var new_data = function(d, msg) {
    var d = _.extend(d, msg)
    insert(d, function (err, res) {
      console.log('saved it!', res)
    })
  }

  var start_recording = function (msg) { 
    console.log('got start') 
    var timeout = msg.duration*1000
    var handler = function (data) { return new_data(data, msg) }
    faucet.bind('mindwave', handler)
    // unsubscribe/unbind after timeout has elapsed
    setTimeout(function () {
      faucet.unbind('mindwave', handler)
      console.log('done! now i will emit, ', msg.success_message)
      post(msg.success_message)
    }, timeout)
  }

  // bind to start-recording event
  faucet.bind('start-recording', start_recording)

})

// create database if we need to
//r.dbCreate('passthoughts').run(conn, console.log)
// create a table if we need to
//r.db('passthoughts').tableCreate('enrollment').run(conn, console.log)
