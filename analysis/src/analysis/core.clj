(ns analysis.database
  (:require [bitemyapp.revise.connection :refer [connect close]]
            [bitemyapp.revise.query :as r]
            [bitemyapp.revise.core :refer [run run-async]]))

;; CONFIG

(def db-config {:host "192.168.99.100" :port 32769})
(def my-db "passthoughts")
(def my-table "enrollment")


;; HELPER FNs

(defn -table-in-db [db-name table-name]
  "Helper function for specifying a table within a db."
  (-> (r/db db-name) (r/table-db table-name)))


;; PUBLIC FUNCTIONS

(def my-db-table
  (-table-in-db my-db my-table))


(defn get-texts-from [number]
  "Gets all texts from a given number."
  (let [conn (connect db-config)
        q    (-> my-db-table
                 (r/get-all [number] :tag)
                 (run conn))]
    (close conn)
    (:response q)))

(get-texts-from "train_sport")
