import pusherclient
from src.pusher_logging import pusher_logging
from config.config import config
from src import brainlib
import numpy as np
import time
import json
global pusher
from sklearn.metrics.pairwise import cosine_similarity

# DEBUG: this side-effecty function sets up a logger that lets us see the raw communications data from pusher
# pusher_logging()

# config
bin_size = 100
reading_resolution = 3

#
raw_buffer = []
last_vector = None

def raw_values (reading):
  return json.loads(reading)[u'reading'][u'raw_values']

def similarity (vector):
  global last_vector
  if last_vector is not None:
    cs = cosine_similarity(vector, last_vector)
    print 'cosine similarity between this reading and last is:'
    print cs
  last_vector = vector

def process_readings (buff):
  '''
  Turns a buffer of readings into a single feature vector.
  It's assumed that the buffer is full of the readings that came in over the wire,
  i.e. that they are unprocessed.
  '''
  # helper fn for getting power spectrum for a reading that comes through the wire
  def pspec (reading):
    return brainlib.pSpectrum(raw_values(reading))
  # list of pspectra for all readings in the buffer
  pspectra = [pspec(r) for r in buff]
  # average power spectrum + log bin to make a feature vector
  vector = brainlib.avgPowerSpectrum(
    brainlib.binnedPowerSpectra(pspectra, bin_size),
    np.log)
  similarity(vector)

def buff (datum):
  global raw_buffer 
  print 'buff length:', len(raw_buffer)
  #
  if len(raw_buffer) == reading_resolution:
    process_readings(raw_buffer)
    raw_buffer = []
  #
  raw_buffer.append(datum)

# We can't subscribe until we've connected, so we use a callback handler
# to subscribe when able
def connect_handler(_):
  channel = pusher.subscribe('everything')
  channel.bind('mindwave', buff)

# connect, call connect_handler on connection
pusher = pusherclient.Pusher(config['PUSHER_KEY'])
pusher.connection.bind('pusher:connection_established', connect_handler)
pusher.connect()

#pusher.disconnect()

while True:
    # Do other things in the meantime here...
    time.sleep(1)
