def pusher_logging():
	"Side-effecty function that adds a handler so we can see the raw communications data from pusher."
	import sys
	import logging
	root = logging.getLogger()
	root.setLevel(logging.INFO)
	ch = logging.StreamHandler(sys.stdout)
	root.addHandler(ch)
