import rethinkdb as r
from sklearn.metrics.pairwise import cosine_similarity
from itertools import combinations
import numpy as np

my_host        = '192.168.99.100'
my_port        = 32769
my_db          = 'passthoughts3'
my_table       = 'enrollment'

conn = r.connect(my_host, my_port)

cursor = r.db(my_db).table(my_table).filter(r.row["user_id"] == "nick").run(conn)

stuff = [thing for thing in cursor]

stuff

raws = [v[u'reading'][u'raw_values'] for v in stuff]

all_pairs = combinations(raws, 2)

sims = [cosine_similarity(pair) for pair in all_pairs]

# self-similarity
np.mean(sims)
# cross-similarity (for auth)
#np.mean(similarities(sims.append(challenge)))


 emit -
self-sim of set (sans new reading)
cross-sim of set (with new reading)
percent difference
new reading type
reading [an array of ids for each reading]
so you can modify downstream



