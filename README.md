## setup

I recommend you start a virtual environment,

`virtualenv .`

then `source bin/activate` and `pip install -r requirements.txt`

## running

To start,

`source bin/activate` 

then

`python cosine_similarity.py`

To see it in action, assuming the indra server is running,

`npm install` then `npm test`
